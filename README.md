<img align="left" width="160" height="160" src="https://codeberg.org/turtle/webclock/raw/branch/main/data/icons/hicolor/scalable/apps/garden.turtle.Webclock.svg">

# Webclock
Use Internet Time comfortably

Webclock allows you to see the current time in the Internet Time standard, and allows you to share a link to the current time.

## Contributing

### Translation

Please fork the project and use PoEdit to create translations. Make sure to base the translation on `webclock.pot`.

### Updating translations

If you changed a translatable string, please run `meson _build --prefix=/usr && meson compile -C _build webclock-update-po` before committing.
