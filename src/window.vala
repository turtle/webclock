/* window.vala
 *
 * Copyright 2023 skilpedde
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk;
using GLib;
using Adw;
namespace Webclock {
    [GtkTemplate (ui = "/garden/turtle/Webclock/ui.ui")] // this currently does not work. TODO: fix this
    public class Window : Adw.ApplicationWindow {
        [GtkChild] public unowned Gtk.Label beattime;
        [GtkChild] public unowned Adw.ToastOverlay toast;
        [GtkChild] private unowned Adw.ViewStack stack;
        [GtkChild] private unowned Adw.ViewStackPage stack1;
        [GtkChild] private unowned Adw.ViewStackPage stack2;

        public Window (Gtk.Application app) {
            Object (
                application: app
            );
        }

        construct {
            ActionEntry[] action_entries = {
                { "copy", this.copy_link }
            };
            this.add_action_entries (action_entries, this);

            this.updateTime (); // instant update of time on start
            this.updater ();
        }

        // convert normal time into internet beat time
        private int convertToBeat (DateTime otime) {
            // get Zürich timezone for use with beat conversion
            TimeZone tz = new TimeZone.offset (3600);

            DateTime time = otime.to_timezone (tz);
            string beat1 = (((time.get_hour () * 3600) + (time.get_minute () * 60) + time.get_second ()) / 86.4).to_string ();
            int beat = int.parse (beat1);
            return beat;
        }

        // format beat time correctly
        private string paddBeat (int beat) {
            if (beat < 10) {
                return "@00" + beat.to_string ();
            } else if (beat < 100) {
                return "@0" + beat.to_string ();
            } else {
                return "@" + beat.to_string ();
            }
        }

        // run the constant updater on the main tab
        private bool updateTime () {
            DateTime now = new DateTime.now_local ();
            string beatNow = paddBeat (convertToBeat (now));
            beattime.set_text (beatNow);
            return true;
        }

        // updates webclock time
        // TODO: sync time better, align it with actual webtime?
        public async void updater () {
            GLib.Timeout.add_full (1, 1000, updateTime);
        }

        // copies link to current time at internet-ti.me
        public void copy_link () {
            Gdk.Clipboard clip = Gdk.Display.get_default ().get_clipboard ();
            string time = beattime.get_label ();
            clip.set_text ("https://internet-ti.me/" + time);

            // Translators: This is an independent sentence with no other context
            Adw.Toast t = new Adw.Toast (_("Copied link"));
            toast.add_toast (t);
        }

        // pad time selector times
        // function from GNOME Clocks — thank you!
        [GtkCallback]
        private bool show_leading_zeros (Gtk.SpinButton spin_button) {
            spin_button.set_text ("%02i".printf (spin_button.get_value_as_int ()));
            return true;
        }

    }
}
